import Admin from './modules/admin'
import Student from './modules/student'
import Teacher from './modules/teacher'
import TheNotFound from './TheNotFound'

const routes = [
  {
    path: '/admin',
    component: Admin,
    children: Admin.routes
  },
  {
    path: '/student',
    component: Student,
    children: Student.routes
  },
  {
    path: '/teacher',
    component: Teacher,
    children: Teacher.routes
  },
  {
    path: '/',
    redirect: '/admin'
  },
  {
    path: '*',
    name: '404',
    component: TheNotFound
  }
]

export default routes
