import http from '@/plugins/http'

export const auditory = {
  getAll (completed, failed) {
    return http.get('auditory/GetAll', completed, failed)
  },
  getById (id, completed, failed) {
    return http.get('auditory/getbyid?id=' + id, completed, failed)
  },
  add (body, completed, failed) {
    return http.post('auditory/add', body, completed, failed)
  },
  update (body, completed, failed) {
    return http.post('auditory/update', body, completed, failed)
  },
  delete (body, completed, failed) {
    return http.post('auditory/delete', body, completed, failed)
  }
}

export const weekSchedule = {
  getAll (completed, failed) {
    return http.get('weekschedule/GetAll', completed, failed)
  },
  getById (id, completed, failed) {
    return http.get('weekschedule/getbyid?id=' + id, completed, failed)
  },
  add (body, completed, failed) {
    return http.post('weekschedule/add', body, completed, failed)
  },
  update (body, completed, failed) {
    return http.post('weekschedule/update', body, completed, failed)
  },
  delete (body, completed, failed) {
    return http.post('weekschedule/delete', body, completed, failed)
  }
}

export const auditoryType = {
  getAll (completed, failed) {
    return http.get('auditorytype/getall', completed, failed)
  }
}

export const building = {
  getAll (completed, failed) {
    return http.get('building/GetAll', completed, failed)
  }
}

export const group = {
  getAll (completed, failed) {
    return http.get('group/GetAll', completed, failed)
  }
}

export const subject = {
  getAll (completed, failed) {
    return http.get('subject/GetAll', completed, failed)
  }
}

export const teacher = {
  getAll (completed, failed) {
    return http.get('teacher/GetAll', completed, failed)
  },
  getById (id, completed, failed) {
    return http.get('teacher/getbyid?id=' + id, completed, failed)
  },
  add (body, completed, failed) {
    return http.post('teacher/add', body, completed, failed)
  },
  update (body, completed, failed) {
    return http.post('teacher/update', body, completed, failed)
  },
  delete (body, completed, failed) {
    return http.post('teacher/delete', body, completed, failed)
  }
}

export const student = {
  getAll (completed, failed) {
    return http.get('student/GetAll', completed, failed)
  },
  getById (id, completed, failed) {
    return http.get('student/getbyid?id=' + id, completed, failed)
  },
  add (body, completed, failed) {
    return http.post('student/add', body, completed, failed)
  },
  update (body, completed, failed) {
    return http.post('student/update', body, completed, failed)
  },
  delete (body, completed, failed) {
    return http.post('student/delete', body, completed, failed)
  }
}

export const scheduleType = {
  getAll (completed, failed) {
    return http.get('scheduleType/GetAll', completed, failed)
  }
}

export const user = {
  getAll (completed, failed) {
    return http.get('user/GetAll', completed, failed)
  }
}
