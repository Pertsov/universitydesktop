import store from '@/store'

const state = {
}

const actions = {
}

const getters = {
}

const mutations = {
}

store.registerModule('teacher', {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
})
