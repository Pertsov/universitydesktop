import store from '@/store'

const state = {
}

const actions = {
}

const getters = {
}

const mutations = {
}

store.registerModule('student', {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
})
