export default {
  constants: {
    defaultModel: {}
  },
  data () {
    return {
      vm: this,
      searchFilter: '',
      creationMode: false,
      model: this.defaultModel,
      modelCopy: this.defaultModel,
      valid: true
    }
  },
  computed: {
    tableData: function () {
      return []
    },
    modelsEqual: function () {
      return Object.is(this.modelCopy, this.model)
    },
    isChanged: function () {
      return !this.modelsEqual || this.creationMode
    }
  },
  methods: {
    getAll () {
      return Promise.resolve()
    },
    getById () {
      return Promise.resolve({})
    },
    add () {
      return Promise.resolve()
    },
    delete () {
      return Promise.resolve()
    },
    update () {
      return Promise.resolve()
    },
    addEvent () {
      this.creationMode = !this.creationMode
      this.model = this.getCopy(this.defaultModel)
      this.modelCopy = this.getCopy(this.defaultModel)
    },
    deleteEvent () {
      this.$refs.confirm.show().then(result => {
        if (result) {
          this.delete(this.model).then(() => {
            this.$notify({
              group: 'app',
              type: 'success',
              title: 'Видалено успішно!'
            })
            this.selectFirst()
          }, error => {
            this.$notify({
              group: 'app',
              type: 'error',
              title: 'Помилка!',
              text: error.message
            })
          })
        }
      })
    },
    saveEvent () {
      if (this.$refs.form.validate()) {
        if (this.creationMode) {
          this.add(this.model).then(result => {
            this.$notify({
              group: 'app',
              type: 'success',
              title: 'Збережено успішно!'
            })
            this.creationMode = false
            this.model = this.getCopy(result)
            this.modelCopy = this.getCopy(result)
          }, error => {
            this.$notify({
              group: 'app',
              type: 'error',
              title: 'Помилка!',
              text: error.message
            })
          })
        } else {
          this.update(this.model).then(result => {
            this.$notify({
              group: 'app',
              type: 'success',
              title: 'Збережено успішно!'
            })
            this.model = this.getCopy(result)
            this.modelCopy = this.getCopy(result)
          }, error => {
            this.$notify({
              group: 'app',
              type: 'error',
              title: 'Помилка!',
              text: error.message
            })
          })
        }
      }
    },
    cancelEvent () {
      this.$refs.form.reset()
      if (this.creationMode) {
        this.selectFirst()
      } else {
        this.model = this.getCopy(this.modelCopy)
      }
      this.creationMode = false
    },
    getCopy (obj) {
      return JSON.parse(JSON.stringify(obj))
    },
    rowSelected (obj) {
      this.model = this.getCopy(obj)
      this.modelCopy = this.getCopy(obj)
      this.getById(obj.Id).then(result => {
        this.model = {
          ...this.model,
          ...this.getCopy(result)
        }
        this.modelCopy = {
          ...this.model,
          ...this.getCopy(result)
        }
      })
    },
    selectFirst () {
      if (this.tableData.length > 0) {
        this.getById(this.tableData[0].Id).then(result => {
          this.model = this.getCopy(result)
          this.modelCopy = this.getCopy(result)
        })
      }
    }
  },
  created () {
    this.getAll().then(() => {
      this.selectFirst()
    })
  }
}
