import store from '@/store'
import { auditory, auditoryType, building } from '@/app/api'

const state = {
  auditoryList: [],
  auditoryTypeList: [],
  buildingList: []
}

const actions = {
  getAll (context) {
    return new Promise((resolve, reject) => {
      auditory.getAll(result => {
        resolve(result)
        context.commit('setAuditoryList', result)
      }, reject)
    })
  },
  getById (context, id) {
    return new Promise((resolve, reject) => {
      auditory.getById(id, resolve, reject)
    })
  },
  getBuildings (context) {
    return new Promise((resolve, reject) => {
      building.getAll(result => {
        resolve(result)
        context.commit('setBuildingList', result)
      }, reject)
    })
  },
  getAuditoryTypes (context) {
    return new Promise((resolve, reject) => {
      auditoryType.getAll(result => {
        resolve(result)
        context.commit('setAuditoryTypeList', result)
      }, reject)
    })
  },
  update (context, payload) {
    const body = {
      Id: payload.Id,
      Amount: payload.Amount,
      Name: payload.Name,
      Description: payload.Description,
      BuildingId: payload.BuildingId,
      TypeId: payload.TypeId
    }
    return new Promise((resolve, reject) => {
      auditory.update(body, result => {
        resolve(result)
        context.commit('updateAuditory', result)
      }, reject)
    })
  },
  add (context, payload) {
    const body = {
      Amount: payload.Amount,
      Name: payload.Name,
      Description: payload.Description,
      BuildingId: payload.BuildingId,
      TypeId: payload.TypeId
    }
    return new Promise((resolve, reject) => {
      auditory.add(body, result => {
        resolve(result)
        context.commit('addAuditory', result)
      }, reject)
    })
  },
  delete (context, payload) {
    const body = {
      Id: payload.Id,
      Amount: payload.Amount,
      Name: payload.Name,
      Description: payload.Description,
      BuildingId: payload.BuildingId,
      TypeId: payload.TypeId
    }
    return new Promise((resolve, reject) => {
      auditory.delete(body, () => {
        resolve()
        context.commit('deleteAuditory', payload.Id)
      }, reject)
    })
  }
}

const getters = {
  auditoryList: state => state.auditoryList,
  buildingKeyValues: state => state.buildingList.map(item => {
    return {
      value: item.Id,
      text: item.Name
    }
  }),
  auditoryTypeKeyValues: state => state.auditoryTypeList.map(item => {
    return {
      value: item.Id,
      text: item.Name
    }
  })
}

const mutations = {
  setAuditoryList (state, list) {
    state.auditoryList = list
  },
  setAuditoryTypeList (state, list) {
    state.auditoryTypeList = list
  },
  setBuildingList (state, list) {
    state.buildingList = list
  },
  addAuditory (state, auditory) {
    const building = state.buildingList.find(b => b.Id === auditory.BuildingId)
    const auditoryType = state.auditoryTypeList.find(at => at.Id === auditory.TypeId)
    state.auditoryList.push({
      Id: auditory.Id,
      Amount: auditory.Amount,
      Name: auditory.Name,
      Description: auditory.Description,
      BuildingName: building ? building.Name : '',
      AuditoryType: auditoryType ? auditoryType.Name : ''
    })
  },
  updateAuditory (state, auditory) {
    const index = state.auditoryList.findIndex(a => a.Id === auditory.Id)
    if (index >= 0) {
      const building = state.buildingList.find(b => b.Id === auditory.BuildingId)
      const auditoryType = state.auditoryTypeList.find(at => at.Id === auditory.TypeId)
      state.auditoryList.splice(index, 1, {
        Id: auditory.Id,
        Amount: auditory.Amount,
        Name: auditory.Name,
        Description: auditory.Description,
        BuildingName: building ? building.Name : '',
        AuditoryType: auditoryType ? auditoryType.Name : ''
      })
    }
  },
  deleteAuditory (state, id) {
    const index = state.auditoryList.findIndex(a => a.Id === auditory.Id)
    if (index >= 0) {
      state.auditoryList.splice(index, 1)
    }
  }
}

store.registerModule(['admin', 'auditory'], {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
})
