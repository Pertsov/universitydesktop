import store from '@/store'
import { weekSchedule, auditory, group, teacher, subject, scheduleType } from '@/app/api'

const state = {
  weekScheduleList: [],
  weekDays: ['Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', "П'ятниця", 'Субота'],
  lessonNumbers: [1, 2, 3, 4, 5, 6],
  teacherList: [],
  groupList: [],
  auditoryList: [],
  scheduleTypeList: [],
  subjectList: []
}

const actions = {
  getAll (context) {
    return new Promise((resolve, reject) => {
      weekSchedule.getAll(result => {
        resolve(result)
        context.commit('setWeekScheduleList', result)
      }, reject)
    })
  },
  getById (context, id) {
    return new Promise((resolve, reject) => {
      weekSchedule.getById(id, resolve, reject)
    })
  },
  getAuditories (context) {
    return new Promise((resolve, reject) => {
      auditory.getAll(result => {
        resolve(result)
        context.commit('setAuditoryList', result)
      }, reject)
    })
  },
  getGroups (context) {
    return new Promise((resolve, reject) => {
      group.getAll(result => {
        resolve(result)
        context.commit('setGroupList', result)
      }, reject)
    })
  },
  getTeachers (context) {
    return new Promise((resolve, reject) => {
      teacher.getAll(result => {
        resolve(result)
        context.commit('setTeacherList', result)
      }, reject)
    })
  },
  getSubjects (context) {
    return new Promise((resolve, reject) => {
      subject.getAll(result => {
        resolve(result)
        context.commit('setSubjectList', result)
      }, reject)
    })
  },
  getScheduleTypes (context) {
    return new Promise((resolve, reject) => {
      scheduleType.getAll(result => {
        resolve(result)
        context.commit('setScheduleTypeList', result)
      }, reject)
    })
  },
  add (context, payload) {
    const body = {
      Day: payload.Day,
      LessonNumber: payload.LessonNumber,
      ScheduleTypeId: payload.ScheduleTypeId,
      AuditoryId: payload.AuditoryId,
      SubjectId: payload.SubjectId,
      TeacherId: payload.TeacherId,
      GroupId: payload.GroupId
    }
    return new Promise((resolve, reject) => {
      weekSchedule.add(body, result => {
        resolve(result)
        context.commit('addWeekSchedule', result)
      }, reject)
    })
  },
  update (context, payload) {
    const body = {
      Id: payload.Id,
      Day: payload.Day,
      LessonNumber: payload.LessonNumber,
      ScheduleTypeId: payload.ScheduleTypeId,
      AuditoryId: payload.AuditoryId,
      SubjectId: payload.SubjectId,
      TeacherId: payload.TeacherId,
      GroupId: payload.GroupId
    }
    return new Promise((resolve, reject) => {
      weekSchedule.update(body, result => {
        resolve(result)
        context.commit('updateWeekSchedule', result)
      }, reject)
    })
  },
  delete (context, payload) {
    const body = {
      Id: payload.Id,
      Day: payload.Day,
      LessonNumber: payload.LessonNumber,
      ScheduleTypeId: payload.ScheduleTypeId,
      AuditoryId: payload.AuditoryId,
      SubjectId: payload.SubjectId,
      TeacherId: payload.TeacherId,
      GroupId: payload.GroupId
    }
    return new Promise((resolve, reject) => {
      weekSchedule.delete(body, () => {
        resolve()
        context.commit('deleteWeekSchedule', payload.Id)
      }, reject)
    })
  }
}

const getters = {
  weekScheduleList: state => state.weekScheduleList.map(item => {
    item.DayName = state.weekDays[item.Day]
    return item
  }),
  auditoryKeyValues: state => state.auditoryList.map(item => {
    return {
      value: item.Id,
      text: item.Name
    }
  }),
  teacherKeyValues: state => state.teacherList.map(item => {
    return {
      value: item.Id,
      text: item.Name
    }
  }),
  subjectKeyValues: state => state.subjectList.map(item => {
    return {
      value: item.Id,
      text: item.Name
    }
  }),
  groupKeyValues: state => state.groupList.map(item => {
    return {
      value: item.Id,
      text: item.Name
    }
  }),
  scheduleTypeKeyValues: state => state.scheduleTypeList.map(item => {
    return {
      value: item.Id,
      text: item.Name
    }
  }),
  weekDaysKeyValues: state => state.weekDays.map((item, index) => {
    return {
      value: index,
      text: item
    }
  }),
  lessonNumberKeyValues: state => state.lessonNumbers.map(item => ({value: item, text: item}))
}

const mutations = {
  setWeekScheduleList (state, list) {
    state.weekScheduleList = list
  },
  setAuditoryList (state, list) {
    state.auditoryList = list
  },
  setSubjectList (state, list) {
    state.subjectList = list
  },
  setGroupList (state, list) {
    state.groupList = list
  },
  setTeacherList (state, list) {
    state.teacherList = list
  },
  setScheduleTypeList (state, list) {
    state.scheduleTypeList = list
  },
  addWeekSchedule (state, weekSchedule) {
    state.weekScheduleList.push(weekSchedule)
  },
  updateWeekSchedule (state, weekSchedule) {
    const index = state.weekScheduleList.findIndex(a => a.Id === weekSchedule.Id)
    if (index >= 0) {
      state.auditoryList.splice(index, 1, weekSchedule)
    }
  },
  deleteWeekSchedule (state, id) {
    const index = state.weekScheduleList.findIndex(a => a.Id === weekSchedule.Id)
    if (index >= 0) {
      state.weekScheduleList.splice(index, 1)
    }
  }
}

store.registerModule(['admin', 'weekschedule'], {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
})
