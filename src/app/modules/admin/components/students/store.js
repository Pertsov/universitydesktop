import store from '@/store'
import { student, user, group } from '@/app/api'

const state = {
  studentList: [],
  userList: [],
  groupList: []
}

const actions = {
  getAll (context) {
    return new Promise((resolve, reject) => {
      student.getAll(result => {
        resolve(result)
        context.commit('setStudentList', result)
      }, reject)
    })
  },
  getById (context, id) {
    return new Promise((resolve, reject) => {
      student.getById(id, resolve, reject)
    })
  },
  getUsers (context) {
    return new Promise((resolve, reject) => {
      user.getAll(result => {
        resolve(result)
        context.commit('setUserList', result)
      }, reject)
    })
  },
  getGroups (context) {
    return new Promise((resolve, reject) => {
      group.getAll(result => {
        resolve(result)
        context.commit('setGroupList', result)
      }, reject)
    })
  },
  add (context, payload) {
    const body = {
      Name: payload.Name,
      RecordBook: payload.RecordBook,
      StudentIdCard: payload.StudentIdCard,
      Email: payload.Email,
      GroupId: payload.GroupId,
      Phone: payload.Phone,
      ApplicationUserId: payload.ApplicationUserId
    }
    return new Promise((resolve, reject) => {
      student.add(body, result => {
        resolve(result)
        context.commit('addStudent', result)
      }, reject)
    })
  },
  update (context, payload) {
    const body = {
      Id: payload.Id,
      Name: payload.Name,
      RecordBook: payload.RecordBook,
      StudentIdCard: payload.StudentIdCard,
      Email: payload.Email,
      GroupId: payload.GroupId,
      Phone: payload.Phone,
      ApplicationUserId: payload.ApplicationUserId
    }
    return new Promise((resolve, reject) => {
      student.update(body, result => {
        resolve(result)
        context.commit('updateStudent', result)
      }, reject)
    })
  },
  delete (context, payload) {
    const body = {
      Id: payload.Id,
      Name: payload.Name,
      RecordBook: payload.RecordBook,
      StudentIdCard: payload.StudentIdCard,
      Email: payload.Email,
      GroupId: payload.GroupId,
      Phone: payload.Phone,
      ApplicationUserId: payload.ApplicationUserId
    }
    return new Promise((resolve, reject) => {
      student.delete(body, () => {
        resolve()
        context.commit('deleteStudent', payload.Id)
      }, reject)
    })
  }
}

const getters = {
  studentList: state => state.studentList,
  userKeyValues: state => state.userList.map(item => {
    return {
      value: item.Id,
      text: item.UserName
    }
  }),
  groupKeyValues: state => state.groupList.map(item => {
    return {
      value: item.Id,
      text: item.Name
    }
  })
}

const mutations = {
  setStudentList (state, list) {
    state.studentList = list
  },
  setUserList (state, list) {
    state.userList = list
  },
  setGroupList (state, list) {
    state.groupList = list
  },
  addStudent (state, student) {
    state.studentList.push(student)
  },
  updateStudent (state, student) {
    const index = state.studentList.findIndex(a => a.Id === student.Id)
    if (index >= 0) {
      state.studentList.splice(index, 1, student)
    }
  },
  deleteStudent (state, id) {
    const index = state.studentList.findIndex(a => a.Id === student.Id)
    if (index >= 0) {
      state.studentList.splice(index, 1)
    }
  }
}

store.registerModule(['admin', 'student'], {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
})
