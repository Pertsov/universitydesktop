import store from '@/store'
import { teacher, user } from '@/app/api'

const state = {
  teacherList: [],
  userList: []
}

const actions = {
  getAll (context) {
    return new Promise((resolve, reject) => {
      teacher.getAll(result => {
        resolve(result)
        context.commit('setTeacherList', result)
      }, reject)
    })
  },
  getById (context, id) {
    return new Promise((resolve, reject) => {
      teacher.getById(id, resolve, reject)
    })
  },
  getUsers (context) {
    return new Promise((resolve, reject) => {
      user.getAll(result => {
        resolve(result)
        context.commit('setUserList', result)
      }, reject)
    })
  },
  add (context, payload) {
    const body = {
      Name: payload.Name,
      Description: payload.Description,
      Email: payload.Email,
      Phone: payload.Phone,
      ApplicationUserId: payload.ApplicationUserId
    }
    return new Promise((resolve, reject) => {
      teacher.add(body, result => {
        resolve(result)
        context.commit('addTeacher', result)
      }, reject)
    })
  },
  update (context, payload) {
    const body = {
      Id: payload.Id,
      Name: payload.Name,
      Description: payload.Description,
      Email: payload.Email,
      Phone: payload.Phone,
      ApplicationUserId: payload.ApplicationUserId
    }
    return new Promise((resolve, reject) => {
      teacher.update(body, result => {
        resolve(result)
        context.commit('updateTeacher', result)
      }, reject)
    })
  },
  delete (context, payload) {
    const body = {
      Id: payload.Id,
      Name: payload.Name,
      Description: payload.Description,
      Email: payload.Email,
      Phone: payload.Phone,
      ApplicationUserId: payload.ApplicationUserId
    }
    return new Promise((resolve, reject) => {
      teacher.delete(body, () => {
        resolve()
        context.commit('deleteTeacher', payload.Id)
      }, reject)
    })
  }
}

const getters = {
  teacherList: state => state.teacherList,
  userKeyValues: state => state.userList.map(item => {
    return {
      value: item.Id,
      text: item.UserName
    }
  })
}

const mutations = {
  setTeacherList (state, list) {
    state.teacherList = list
  },
  setUserList (state, list) {
    state.userList = list
  },
  addTeacher (state, teacher) {
    state.teacherListList.push(teacher)
  },
  updateTeacher (state, teacher) {
    const index = state.teacherList.findIndex(a => a.Id === teacher.Id)
    if (index >= 0) {
      state.teacherList.splice(index, 1, teacher)
    }
  },
  deleteTeacher (state, id) {
    const index = state.teacherList.findIndex(a => a.Id === teacher.Id)
    if (index >= 0) {
      state.teacherList.splice(index, 1)
    }
  }
}

store.registerModule(['admin', 'teacher'], {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
})
