export default [
  {
    path: '',
    redirect: 'auditories'
  },
  {
    path: 'auditories',
    component: () => import('./components/auditories')
  },
  {
    path: 'teachers',
    component: () => import('./components/teachers')
  },
  {
    path: 'students',
    component: () => import('./components/students')
  },
  {
    path: 'weekschedules',
    component: () => import('./components/weekschedules')
  }
]
