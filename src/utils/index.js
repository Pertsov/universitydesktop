global.valueEquals = function (a, b) {
  if (a !== null && b !== null && a !== undefined && b !== undefined && Array.isArray(a) && Array.isArray(b)) {
    return a.length === b.length && a.every((value, index) => { return funcObjectsAreSame(value, b[index]) })
  }
  return (a == null && b == null) || (a != null && b != null && a.toString() === b.toString())
}

function funcObjectsAreSame (x, y) {
  var objectsAreSame = true
  for (var propertyName in x) {
    if (typeof x[propertyName] === 'object') {
      if (!funcObjectsAreSame(x[propertyName], y[propertyName])) {
        objectsAreSame = false
        break
      }
    } else {
      if (x[propertyName] !== y[propertyName]) {
        objectsAreSame = false
      }
    }
  }
  return objectsAreSame
}
