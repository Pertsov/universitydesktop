import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://localhost:44359/api/',
  // baseURL: 'https://universityapi.azurewebsites.net/api/',
  headers: {
    // 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbkB1bml2ZXJzaXR5LmNvbSIsImp0aSI6IjI2OWRiMzA2LWRmMTktNDc4NS1iMjYwLWQ0YTNlMTFhNDY4YyIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJhN2RlMzZkMC0zOWY3LTQyOGUtYWM4MS0xN2UxMTVhZTZiYmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJBZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTQ1MDcyNDIwLCJpc3MiOiJVbml2ZXJzaXR5QVBJQXV0aFNlcnZlciIsImF1ZCI6IlVuaXZlcnNpdHlBUElBdXRoU2VydmVyIn0.l6Xx40WnLuhhz06zDLQNmq6FRTu9PLh8w0KwxoG1OhA'
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbkB1bml2ZXJzaXR5LmNvbSIsImp0aSI6IjFlNzc3NjAzLWQ2YjctNGUzMS1hMWMzLTM2Nzc3MTRiMGUzYyIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJhN2RlMzZkMC0zOWY3LTQyOGUtYWM4MS0xN2UxMTVhZTZiYmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJBZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTQ1MDc1ODI1LCJpc3MiOiJVbml2ZXJzaXR5QVBJQXV0aFNlcnZlciIsImF1ZCI6IlVuaXZlcnNpdHlBUElBdXRoU2VydmVyIn0.Ti0rZyhEYHG6SUU-CMeihuVUTtG4XYeQvhDEid0GECU'
  }
})

function _call (config, completed, failed) {
  return instance(config)
    .then(result => {
      if (completed) completed(result.data)
    })
    .catch(error => {
      failed(error)
      // switch (error.response.status) {
      //   case 400: // Bad Request
      //   case 401: // Unauthorized
      //     // auth.renewToken()
      //     //   .then(() => {
      //     //     instance(config)
      //     //       .then(result => {
      //     //         if (completed) completed(result.data)
      //     //       })
      //     //       .catch(failed || (() => {}))
      //     //   })
      //     //   .catch(failed || (() => {}))
      //     break
      //   case 403: // Forbidden
      //     if (failed) failed(error)
      //     break
      //   default:
      //     if (failed) failed(error)
      //     // if (DEV_ENV) console.log(error)
      // }
    })
}

function get (url, completed, failed) {
  return _call({
    method: 'GET',
    url: url
  }, completed, failed)
}

function post (url, data, completed, failed) {
  return _call({
    method: 'POST',
    url: url,
    data: data
  }, completed, failed)
}

function waitAll (requests, completed, failed) {
  return axios.all(requests)
    .then(completed || (() => {}))
    .catch(failed || (() => {}))
}

export default {
  get,
  post,
  waitAll
}
