import Vue from 'vue'
import App from './app/App'
import DaySpanVuetify from 'dayspan-vuetify'
import VueConstants from 'vue-constants'
import Notifications from 'vue-notification'

import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'dayspan-vuetify/dist/lib/dayspan-vuetify.min.css'

import router from './router'
import store from './store'
import './utils'

Vue.config.productionTip = false

Vue.use(VueConstants)
Vue.config.optionMergeStrategies.constants = (toVal, fromVal) => {
  return {
    ...toVal || {},
    ...fromVal
  }
}

Vue.use(Notifications)

Vue.use(DaySpanVuetify, {
  methods: {
    getDefaultEventColor: () => '#1976d2'
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
